/// <reference types="cypress" />

import { LoginPage } from "../page-objects/loginpage"
import { HomePage } from "../page-objects/homepage"
import { TimeFrame } from "../page-objects/timeframe"
const loginPage = new LoginPage()
var homePage = new HomePage()
var timeframe = new TimeFrame()

describe.only('verify login scenarios', () => {
    before(() => {
        homePage = loginPage.login("global.atester")
    })

    it('should logout from AlphaSense home page', () => {
        homePage.isSearchButtonVisible()
        homePage.logout()
        loginPage.isLoginPageVisible()
    })
})

describe('Filter clearance scenarios', () => {
    beforeEach(() => {
        loginPage.login("global.atester")
    })

    it('validating an invalid ticker should throw an error', () => {
        homePage.enterTickerAndPressEnter("ASDFG")
        cy.get('.text').should('have.text', "ASDFG is not a valid ticker or company name. Please try again.")
        homePage.clearAll()
        homePage.verifyTickerIsEmpty()
    })

    it('verify clear keyword and clear all button clicks', () => {
        let keyword = 'apple iphone'
        let sector = 'Energy'
        let region = 'Europe'

        // clear keywords
        homePage.enterKeywordAndSearch(keyword)
        homePage.verifySearchedKeyword(keyword)
        homePage.clearKeywords()
        homePage.verifyKeywordIsEmpty()

        // make some filter selection and clear keyword only
        homePage.selectIndustry(sector)
        homePage.selectCountry(region)
        timeframe.selectLast7Days()
        homePage.selectFirstTag()
        homePage.enterKeywordAndSearch(keyword)
        homePage.clearKeywords()
        homePage.verifyKeywordIsEmpty()
        homePage.verifyTickerIsEmpty()
        homePage.verifySelectedCountry(region)
        homePage.verifySelectedIndustry(sector)
        homePage.verifySelectedTimeframe('Last 7 Days')
        homePage.verifySelectedTagIsNotAll()

        // enter keyword again and clear all
        homePage.enterKeywordAndSearch(keyword)
        homePage.clearAll()
        homePage.verifyKeywordIsEmpty()
        homePage.verifyTickerIsEmpty()
        homePage.verifySelectedCountry('All')
        homePage.verifySelectedIndustry('All')
        homePage.verifySelectedTimeframe('Last 2 Years')
        homePage.verifySelectedTag('Any')
    })
})

describe('Results grid access scenarios', () => {
    before(() => {
        loginPage.login("global.atester")
    })

    it('click the first document from results grid', () => {
        const resultsGrid = homePage.clickSearchButton()
        resultsGrid.clickFirstRow()
    })
})

describe('Document header panel visibility scenarios', () => {
    beforeEach(() => {
        loginPage.login("global.atester")
    })

    it('public document header panel buttons visibility test', () => {
        homePage.selectSource('Press Releases')
        var resultsGrid = homePage.clickSearchButton()
        var documentHeader = resultsGrid.clickFirstRow()
        assertDocumentHeaderButtonsVisibility(documentHeader)

        resultsGrid = homePage.clickSearchButton()
        assertDocumentHeaderButtonsInvisibility(documentHeader)
    })

    it('user document header panel buttons visibility test', () => {
        homePage.selectSource('My Documents')
        var resultsGrid = homePage.clickSearchButton()
        var documentHeader = resultsGrid.clickFirstRow()
        assertDocumentHeaderButtonsVisibility(documentHeader)
        documentHeader.isShareButtonVisible()

        resultsGrid = homePage.clickSearchButton()
        assertDocumentHeaderButtonsInvisibility(documentHeader)
        cy.get(documentHeader.shareButton).should('not.be.visible')
    })
})

describe('Document header panel visibility scenarios for RSS Feed user', () => {
    beforeEach(() => {
        loginPage.login("rssenabled.atester")
    })

    it('rss document header panel buttons visibility test', () => {
        homePage.selectSource('RSS Feeds')
        var resultsGrid = homePage.clickSearchButton()
        var documentHeader = resultsGrid.clickFirstRow()
        assertDocumentHeaderButtonsVisibility(documentHeader)

        resultsGrid = homePage.clickSearchButton()
        assertDocumentHeaderButtonsInvisibility(documentHeader)
    })
})

describe('Tooltip verification scenarios', () => {
    beforeEach(() => {
        loginPage.login("global.atester")
    })
})

function assertDocumentHeaderButtonsVisibility(documentHeader) {
    documentHeader.isBulkHighlightButtonVisible()
    documentHeader.isTagsButtonVisible()
    documentHeader.isBookmarkButtonVisible()
    documentHeader.isDocLinkButtonVisible()
    documentHeader.isPrintButtonVisible()
    documentHeader.isDownloadButtonVisible()
    documentHeader.isBipSyncButtonVisible()
}

function assertDocumentHeaderButtonsInvisibility(documentHeader) {
    cy.get(documentHeader.bulkHighlightButton).should('not.be.visible')
    cy.get(documentHeader.tagsButton).should('not.be.visible')
    cy.get(documentHeader.bookmarkButton).should('not.be.visible')
    cy.get(documentHeader.docLinkButton).should('not.be.visible')
    cy.get(documentHeader.printButton).should('not.be.visible')
    cy.get(documentHeader.downloadButton).should('not.be.visible')
    cy.get(documentHeader.bipSyncButton).should('not.be.visible')
}