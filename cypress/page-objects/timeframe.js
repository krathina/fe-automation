export class TimeFrame {
    selectLast7Days() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter0').click()
    }
    selectLast30Days() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter1').click()
    }

    selectLast90Days() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter2').click()
    }

    selectLast6Months() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter3').click()
    }

    selectLast12Months() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter4').click()
    }

    selectLast18Months() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter5').click()
    }

    selectLast2Years() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter6').click()
    }

    selectAllDates() {
        cy.get('#timeframe').click()
        cy.get('#dateFilter7').click()
    }

    selectCustomDateRange() {
        cy.get('#timeframe').click()
        cy.get('#customDateFilter').click()
    }
}