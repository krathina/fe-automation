import { HomePage } from "./homepage"

export class LoginPage {
    navigate() {
        cy.visit('https://rc.alpha-sense.com')
    }

    enterUsername(username) {
        cy.get('#username').type(username)
    }

    enterPassword(password) {
        cy.get('#password').type(password)
    }

    clickSignIn() {
        cy.get('#submit').click()
    }
    clickNext() {
        cy.get('#next-step').click()
    }

    login(username) {
        this.navigate()
        this.enterUsername(username)
        this.clickNext()
        this.enterPassword('AlphaAutoPass123!')
        this.clickSignIn()

        return new HomePage()
    }

    isLoginPageVisible() {
        cy.get('.as-login_step-userid > .login-intro > h2').should('have.text', 'Sign In')
    }
}