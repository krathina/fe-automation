export class DocumentHeader {

    bulkHighlightButton = '#docheader-bulk-highlight-button'
    tagsButton = '#docheader-tags-button'
    bookmarkButton = '#doc-header-btn-bookmark'
    shareButton = '#docheader-share-button'
    docLinkButton = '#docheader-email-button'
    printButton = '#docheader-print-button'
    downloadButton = '#docheader-download-button'
    bipSyncButton = '.action-button.bipsync-button'

    // button clicks
    clickBulkHighlightButton() {
        cy.get(this.bulkHighlightButton).click()
    }

    clickTagsButton() {
        cy.get(this.tagsButton).click()
    }

    clickBookmarkButton() {
        cy.get(this.bookmarkButton).click()
    }

    clickShareButton() {
        cy.get(this.shareButton).click()
    }

    clickDocLinkButton() {
        cy.get(docLinkButton).click()
    }

    clickPrintButton() {
        cy.get(this.printButton).click()
    }

    clickDownloadButton() {
        cy.get(this.downloadButton).click()
    }

    clickBipSyncButton() {
        cy.get(this.bipSyncButton).click()
    }

    // visibility check
    isBulkHighlightButtonVisible() {
        cy.get(this.bulkHighlightButton).should('be.visible')
    }

    isTagsButtonVisible() {
        cy.get(this.tagsButton).should('be.visible')
    }

    isBookmarkButtonVisible() {
        cy.get(this.bookmarkButton).should('be.visible')
    }

    isShareButtonVisible() {
        cy.get(this.shareButton).should('be.visible')
    }

    isDocLinkButtonVisible() {
        cy.get(this.docLinkButton).should('be.visible')
    }

    isPrintButtonVisible() {
        cy.get(this.printButton).should('be.visible')
    }

    isDownloadButtonVisible() {
        cy.get(this.downloadButton).should('be.visible')
    }

    isBipSyncButtonVisible() {
        cy.get(this.bipSyncButton).should('be.visible')
    }

    isBulkHighlightButtonHidden() {
        cy.get(this.bulkHighlightButton).should('not.be.visible')
    }

    isTagsButtonHidden() {
        cy.get(this.tagsButton).should('not.be.visible')
    }

    isBookmarkButtonHidden() {
        cy.get(this.bookmarkButton).should('not.be.visible')
    }

    isDocLinkButtonHidden() {
        cy.get(this.docLinkButton).should('not.be.visible')
    }

    isPrintButtonHidden() {
        cy.get(this.printButton).should('not.be.visible')
    }

    isDownloadButtonHidden() {
        cy.get(this.downloadButton).should('not.be.visible')
    }

    isBipSyncButtonHidden() {
        cy.get(this.bipSyncButton).should('not.be.visible')
    }
}