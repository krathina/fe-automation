export class FilterComponent {
    selectType(type) {
        cy.contains(type).parent().parent().find('a.select-only').click({ force: true })
    }
}