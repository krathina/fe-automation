import { FilterComponent } from "./filtercomponent"
import { ResultsGrid } from "./resultsgrid"

const filterComponent = new FilterComponent()

export class HomePage {
    // functions
    logout() {
        this.clickSettingsButton()
        cy.contains("Logout").click()
    }

    selectSource(source) {
        cy.get('#sources').click()
        filterComponent.selectType(source)
    }

    selectIndustry(industry) {
        cy.get('#sector').click()
        filterComponent.selectType(industry)
    }

    selectCountry(country) {
        cy.get('#countries').click()
        filterComponent.selectType(country)
    }

    selectFirstTag() {
        cy.get('#tag-filter').click()
        cy.get('[data-index="0"]').find('input').click()
    }

    clearKeywords() {
        cy.get('.header-container > .reset-icon').click()
    }

    clearTickers() {
        cy.get('.tokenizing-field-wrap > .reset-icon').click()
    }

    clearAll() {
        cy.get('#clearSearch').click()
    }

    verifySearchedKeyword(keyword) {
        cy.get('.CodeMirror-line > span').should('have.text', keyword)
    }

    verifySelectedIndustry(industry) {
        cy.get('#sector > .filter-button > :nth-child(2)').should('have.text', industry)
    }

    verifySelectedCountry(country) {
        cy.get('#countries > .filter-button > :nth-child(2)').should('have.text', country)
    }

    verifySelectedTimeframe(timeframe) {
        cy.get('#timeframe > .filter-button > :nth-child(2)').should('have.text', timeframe)
    }

    verifySelectedTag(tag) {
        cy.get('#tag-filter > .filter-button > :nth-child(2)').should('have.text', tag)
    }

    verifySelectedTagIsNotAll() {
        cy.get('#tag-filter > .filter-button > :nth-child(2)').should('not.have.text', 'All')
    }

    verifyKeywordIsEmpty() {
        cy.get('.CodeMirror-placeholder').should('have.text', 'Search Keywords (Type ? for Help)')
    }

    verifyTickerIsEmpty() {
        cy.get('.tokenizing-field-input')
        .find('input')
        .should('have.attr', 'placeholder', 'Search Ticker or Companies')
    }

    // Button clicks
    clickSettingsButton() {
        cy.get('#settingsButton > .dropdown-menu > .dropdown-button > .icon').click()
    }

    clickSearchButton() {
        cy.get('#search').click()
        return new ResultsGrid()
    }

    // Visibility checks
    isSearchButtonVisible() {
        cy.get('#search > .button').should('be.visible')
    }

    // Entering inputs
    enterTickerAndPressEnter(ticker) {
        cy.get('div#toolbar-ticker-box input[type="text"]').type(ticker + '{Enter}')
    }

    enterKeywordAndSearch(keyword) {
        cy.get('.menu.closed').find('textarea').type(keyword, {force: true})
        this.clickSearchButton()
    }
}