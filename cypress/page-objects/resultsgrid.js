import { DocumentHeader } from "./documentheader"

export class ResultsGrid {
    clickFirstRow() {
        cy.get('[aria-rowindex="1"]').click()
        return new DocumentHeader()
    }
}